//1 функція зворотнього виклику для кожного елементу массива 
//2 let arr = [1, 2, 3, 4, 5];
//    2.1)arr.splice(0, arr.length);
//    2.2)arr.length = 0;  
//    2.3)arr = [];
//3 методом Array.isArray() , він показує тру або фолс


function filterBy(arr, dataType) {
  const filteredArr = [];

  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] !== dataType) {
      filteredArr.push(arr[i]);
    }
  }

  console.log(filteredArr); 
}

filterBy(['hello', 'world', 23, '23', null], 'string');